<?php

namespace Tests\Feature;

use App\Client;
use App\Order;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RequestOrderTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function clients_can_request_order()
    {
        $this->withoutExceptionHandling();

        $response = $this->json('POST', "/orders/", $this->getDefaultOrderRequestFields());

        $response->assertStatus(201);

        $client = Client::first();

        $this->assertDatabaseHas('orders', [
            'client_id' => $client->id,
            'address' => 'Tatooine 24, 2ºB',
            'delivery_date' => Carbon::createFromFormat('d/m/Y', '04/05/2018'),
            'delivery_time_start' => '09:00',
            'delivery_time_end' => '12:00',
        ]);
    }

    /** @test */
    public function clients_are_created_on_request_order()
    {
        $this->withoutExceptionHandling();

        $this->json('POST', "/orders/", $this->getDefaultOrderRequestFields());

        $this->assertDatabaseHas('clients', [
            'firstname' => 'Darth',
            'lastname' => 'Vader',
            'email' => 'darthvader@empire.com',
            'phone' => '45612378',
        ]);
    }

    /** @test */
    public function clients_are_not_recreated_on_request_order_if_exists()
    {
        $this->withoutExceptionHandling();

        $clientEmail = $this->getDefaultOrderRequestFields()['email'];
        factory(Client::class)->create(['email' => $clientEmail]);

        $this->json('POST', "/orders/", $this->getDefaultOrderRequestFields());

        $this->assertSame(1, Client::count());
    }

    /** @test */
    public function client_cant_request_order_without_all_required_fields()
    {
        $response = $this->json('POST', "/orders/", $this->getDefaultOrderRequestFields([
            'firstname' => '',
            'lastname' => '',
            'email' => '',
            'phone' => '',
            'address' => '',
            'delivery_date' => '',
            'delivery_time_start' => '',
            'delivery_time_end' => '',
        ]));

        $response->assertStatus(422);

        $response->assertJsonValidationErrors([
            'firstname',
            'lastname',
            'email',
            'phone',
            'address',
            'delivery_date',
            'delivery_time_start',
            'delivery_time_end',
        ]);

        $this->assertSame(0, Order::count());
    }

    /** @test */
    public function client_cant_request_order_with_wrong_format_fields()
    {
        $response = $this->json('POST', "/orders/", $this->getDefaultOrderRequestFields([
            'email' => 'wrongemail',
            'delivery_date' => '36/4/20166',
            'delivery_time_start' => '122',
            'delivery_time_end' => '122',
        ]));

        $response->assertStatus(422);

        $response->assertJsonValidationErrors([
            'email',
            'delivery_date',
            'delivery_time_start',
            'delivery_time_end',
        ]);

        $this->assertSame(0, Order::count());
    }

    /** @test */
    public function client_cant_request_order_with_delivery_time_end_earlier_than_start()
    {
        $response = $this->json('POST', "/orders/", $this->getDefaultOrderRequestFields([
            'delivery_time_start' => '10:00',
            'delivery_time_end' => '09:00',
        ]));

        $response->assertStatus(422);

        $response->assertJsonValidationErrors([
            'delivery_time_end',
        ]);

        $this->assertSame(0, Order::count());
    }

    /** @test */
    public function client_cant_request_order_with_delivery_time_more_than_max()
    {
        $response = $this->json('POST', "/orders/", $this->getDefaultOrderRequestFields([
            'delivery_time_start' => '09:00',
            'delivery_time_end' => '17:01', // 8h, 1m (max 8h)
        ]));

        $response->assertStatus(422);

        $response->assertJsonValidationErrors([
            'delivery_time_end',
        ]);

        $this->assertSame(0, Order::count());
    }
}
