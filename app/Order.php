<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['client_id', 'address', 'delivery_date', 'delivery_time_start', 'delivery_time_end'];

    protected $dates = ['created_at', 'updated_at', 'delivery_date'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($order) {
            $order->driver()->associate(Driver::inRandomOrder()->first());
        });
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function scopeInDate($query, $date)
    {
        $deliveryDate = Carbon::createFromFormat('d/m/Y', $date)->toDateString();
        return $query->whereDate('delivery_date', '=', $deliveryDate);
    }

    public function scopeOrderByTimeRange($query)
    {
        return $query->orderBy('delivery_time_start', 'ASC');
    }
}
