<?php

namespace Tests\Feature;

use App\Driver;
use App\Order;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DriverAssignmentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function driver_is_assigned_ramdonly_on_order_request()
    {
        $this->withoutExceptionHandling();

        $drivers = factory(Driver::class, 10)->create();

        $this->json('POST', "/orders/", $this->getDefaultOrderRequestFields());
        $order = Order::first();

        $this->assertTrue($drivers->contains(function($driver) use ($order) {
            return $driver->orders->contains($order);
        }));
    }
}
