<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'firstname' => $this->client->firstname,
            'lastname' => $this->client->lastname,
            'phone' => $this->client->phone,
            'address' => $this->address,
            'delivery_date' => $this->delivery_date->format('d/m/Y'),
            'delivery_time_start' => $this->delivery_time_start,
            'delivery_time_end' => $this->delivery_time_end,
        ];
    }
}
